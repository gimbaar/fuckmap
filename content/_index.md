---
layout: home
title: FNIT-Gruppen mit Interesse an Technik
---

Hier findest du die uns bekannten momentan aktiven feministischen
 <abbr title="frauen, non-binary, trans, inter">FNTI</abbr> Gruppen mit Interesse an Technik.
 Die Map vergrößern kannst du, in dem du in sie klickst und das "+" drückst. Klickst du die Marker an, erscheinen dort mögliche Kontaktwege der einzelnen Gruppen.
 Befindet sich in deiner Nähe keine, gibt es unten eine Anleitung zur Gründung deiner eigenen inklusive Kontaktmöglichkeiten zu uns.
 Viel Spaß und bis bald! 
 
 ([Page](https://gitlab.com/melzai/fuckmap) based on [FUCK map](https://gitlab.com/lislis/fuckmap))